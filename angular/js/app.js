// create our angular app and inject ngAnimate and ui-router 
// =============================================================================
var MainApp = angular.module('MainApp', ['ui.router','ngMessages', 'SignupCtrl', 'LoginCtrl','PasswordCtrl','ValidationCtrl'])

// configuring our routes 
// =============================================================================
MainApp.config(function($stateProvider, $urlRouterProvider, $httpProvider) {

    //$urlRouterProvider.otherwise('/signup/setup');

    //$urlRouterProvider.otherwise('/')
    
    //$urlRouterProvider.when('login','templates/login.html');
    //used to prevents cors on angualarjs
    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
    $httpProvider.defaults.headers.common["Accept"] = "application/json";
    $httpProvider.defaults.headers.common["Content-Type"] = "application/json";
    
   
    $stateProvider
    
       
        .state('login', {
            url: '/login',
            templateUrl: 'login.html',
            controller: 'LoginController'
        })

        .state('forgot', {
            url: '/password/forgot',
            templateUrl: 'forgot.html',
            controller: 'PasswordController'
            
        })

        .state('forgot_sent', {
            url: '/password/forgot/sent',
            templateUrl: 'forgot_sent.html'
            
        })

        .state('reset_password', {
            url: '/password/forgot/reset',
            templateUrl: 'reset_password.html',
            controller: 'PasswordController'
        })
        // route to show our basic form (/signup)
        .state('signup', {
            abstract: true,
            url: '/signup',
            templateUrl: 'signup.html',
            controller: 'SignupController'
            
        })
        
        // nested states 
        // each of these sections will have their own view
        // url will be nested (/signup/setup)

       .state('setup', {
            url: '',
            templateUrl: 'signup-setup.html',
            controller: 'ValidationController'
            
        })
        .state('email', {
            url: '/email',
            templateUrl: 'signup-email.html'
        })
        
        // url will be /signup/mobile
        /*.state('mobile', {
            url: '/signup/mobile',
            templateUrl: 'signup-mobile.html'
        })
        */
    //$urlRouterProvider.otherwise('login');

})






