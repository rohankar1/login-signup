var password = angular.module('PasswordCtrl',['ngMessages']);
password.controller('PasswordController', ['$http', '$scope', '$rootScope', '$location', '$rootScope' ,'$state', '$log', '$window',
 function($http, $scope, $rootScope, $location, $state, $log, $window){
    $scope.forgotPassword = function() {

        $scope.forgotMessage = false;
        $rootScope.sentmail = $scope.email;

            $http({
                method  : 'POST',
                url     : 'http://localhost:8000/api/password/email',
                data    : { email:$scope.email } ,
                headers : { 'Content-Type': 'application/json',
                            'Access-Control-Allow-Origin': '*',
                            'Access-Control-Allow-Methods' : 'GET,POST,OPTIONS', }  // set the headers so angular passing info as form data (not request payload)
            }).then(function(response){
                //successfully posted data
                console.log("Email link to reset password has been sent to registered email id", response);

                $window.location.href="http://localhost/repos/angular/templates/forgot_sent.html";
            }, function(response){
                //error has occurred
                console.log("There was an error in the registered email", response);
                $scope.forgotMessage = true;
            })

        }; 
    $scope.resetPassword = function() {

       
        $scope.resetMessage = false;
        //$scope.user= {};


            $http({
                method  : 'POST',
                url     : 'http://localhost:8000/api/password/reset',
                data    : { token:$scope.token, email: $scope.email, password: $scope.password, password_confirmation:$scope.password_confirmation} ,
                headers : { 'Content-Type': 'application/x-www-form-urlencoded',
                            'Access-Control-Allow-Origin': '*'
                 }  // set the headers so angular passing info as form data (not request payload)
            }).then(function(response){
                //successfully posted data
                console.log("Password Reset successfully", response);
                $state.go('login');
            }, function(response){
                //error has occurred
                $scope.resetMessage = true;
                console.log("Error in resetting the password", response);
            })


        }; 

}]);
