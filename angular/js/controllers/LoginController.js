//repos\signup\vendor\laravel\framework\src\Illuminate\Foundation\Auth\AuthenticatesUsers.php 
var login = angular.module('LoginCtrl',['ngMessages']);
login.controller('LoginController', ['$http', '$scope', '$location', '$state', '$log', '$window',
 function($http, $scope, $location, $state, $log, $window){
    $scope.loginSubmit = function() {
   	
    	$scope.authMessage = false;
    	

            $http({
                method  : 'POST',
                url     : 'http://localhost:8000/api/auth/login',
                data    : { email:$scope.email, password:$scope.password} ,
                headers : { 'Content-Type': 'application/json',
                            'Access-Control-Allow-Origin': '*', }  // set the headers so angular passing info as form data (not request payload)
            }).then(function(response){
                //successfully posted data
                console.log("successfully getting auth status", response);
                $window.location.href="http://localhost/repos/angular/templates/loggedin.html";
            }, function(response){
                //error has occurred
                //$scope.auth = true;
                $scope.authMessage = true;
                console.log("There was an error logging in", response);
                
            })


        }; 

        $scope.logout = function() {

         $http({
                method  : 'GET',
                url     : 'http://localhost:8000/api/auth/logout',
                headers : { 'Content-Type': 'application/json',
                            'Access-Control-Allow-Origin': '*',
                            'Access-Control-Allow-Methods' : 'GET,POST,OPTIONS', } 
            }).then(function(response){
                //successfully posted data
                console.log("successfully logged out", response);
                $window.location.href="http://localhost/repos/angular/templates/login.html";
            }, function(response){
                //error has occurred
                //$scope.auth = true;
                
                console.log("There was an error logging out", response);
                
                
            })
        }; 





}]);