//repos\signup\vendor\laravel\framework\src\Illuminate\Foundation\Auth\RegistersUsers.php 
var register = angular.module('SignupCtrl',[]);
register.controller('SignupController', ['$http', '$scope','$rootScope', '$location', '$state', '$log','$timeout',
 function($http, $scope, $rootScope, $location, $state, $log, $timeout){
    $scope.signupSubmit = function() {


        $scope.signupMessage = false;
        $rootScope.sentmail = $scope.email;

            $http({
                method  : 'POST',
                url     : 'http://localhost:8000/api/auth/register',
                data    : { first_name:$scope.first_name, last_name:$scope.last_name, email: $scope.email, mobile_number: $scope.mobile_number, password:$scope.password, role_name:$scope.role_name, company_name: $scope.company_name, country:$scope.country } ,
                //data    :'name=' + $scope.user.name + '&email=' + $scope.user.email  // pass in data as strings
                //data    : param($scope.user),
                headers : { 'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*' }  // set the headers so angular passing info as form data (not request payload)
            }).then(function(response){
                //successfully posted data
                
                console.log("Successfully registered user", response);
                $state.go('email');
            }, function(response){
                //error has occurred
                if (response.status == 422) {
                        $scope.signupMessage = true;
                        $rootScope.submitted = true;
                        $timeout(function () {
                            $scope.signupMessage = false;
                        }, 5000);
                    } else {
                        // invalid response
                        console.log("There was some other error", response);
                    }
                console.log("There was an error in registering user", response);
                
            })


        }; 





}]);