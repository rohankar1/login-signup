<?php
//ALMOST DEFAULT LARAVEL CONTROLLER
namespace App\Http\Controllers\Auth;


use Response;
use Auth;
use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use App\Mailers\AppMailer;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    //protected $redirectPath = '/dash1';
    //protected $redirectAfterLogout = 'api/auth/login';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }


     public function getToken()
    {
        return csrf_token();
    }
    //$token = str_random(30); 
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {

        return Validator::make($data, [
            'first_name' => 'required|max:255', //these are the form names
            'last_name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'mobile_number' => 'required|max:10|unique:users',
            'password' => 'required|confirmed',
            'company_name' => 'required|max:255',
            'role_name' => 'required|max:255',
            'country' => 'required|max:255',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'first_name' => $data['first_name'], //this side is the database columns
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'mobile_number'=> $data['mobile_number'],
            'password' => bcrypt($data['password']),
            'company_name'=> $data['company_name'],
            'role_name'=> $data['role_name'],
            'country'=> $data['country'],
        ]);

       
        
    }


    public function postRegister(Request $request, AppMailer $mailer)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required'
        ]);

        $user = User::create($request->all());

        $mailer->sendEmailConfirmationTo($user);


        return Response::json(array(
            'error' => false,
            'message' => 'Registration Successfull'),
            200
        );
    }

    /**
     * Confirm a user's email address.
     *
     * @param  string $token
     * @return mixed
     */
    public function confirmEmail($token)
    {
        User::whereToken($token)->firstOrFail()->confirmEmail();

        

        return redirect()->away('http://localhost/repos/angular/templates/login.html');
    }

    public function postLogin(Request $request)
    {
        $this->validate($request, ['email' => 'required|email', 'password' => 'required']);
        // Returns response with validation errors if any, and 422 Status Code (Unprocessable Entity)

        if ($this->signIn($request)) {
            

           return response(array('msg' => 'Login Successful'), 200) // 200 Status Code: Standard response for successful HTTP request
              ->header('Content-Type', 'application/json');
        }

        else {
            return response(array('msg' => $this->getFailedLoginMessage()), 401) // 400 Status Code: Forbidden, needs authentication
              ->header('Content-Type', 'application/json');
        }

        

    }

    /**
     * Destroy the user's current session.
     *
     * @return \Redirect
     */
    public function logout()
    {
        Auth::logout();

        //flash('You have now been signed out. See ya.');
        return Response::json(array(
            'error' => false,
            'message' => 'Log Out successful'),
            200
        );

    }

    /**
     * Attempt to sign in the user.
     *
     * @param  Request $request
     * @return boolean
     */
    protected function signIn(Request $request)
    {
        return Auth::attempt($this->getCredentials($request), $request->has('remember'));
    }

    /**
     * Get the login credentials and requirements.
     *
     * @param  Request $request
     * @return array
     */
    protected function getCredentials(Request $request)
    {
        return [
            'email'    => $request->input('email'),
            'password' => $request->input('password'),
            'verified' => true
        ];
    }
}
