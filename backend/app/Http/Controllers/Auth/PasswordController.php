<?php

namespace App\Http\Controllers\Auth;

use Auth;
use App\User;

use Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Password;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;




class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
    public function getToken()
    {
        return csrf_token();
    }

   public function postEmail(Request $request)
    {
        $this->validate($request, ['email' => 'required|email']);

    $response = Password::sendResetLink($request->only('email'), function (Message $message) {
            $message->subject($this->getEmailSubject());
        });

        switch ($response) {
            case Password::RESET_LINK_SENT:
                /*return response(array('msg' => 'Login Successful'), 200)
                ->header('Content-Type', 'application/json');*/
                return Response::json(array(
            'status' => trans($response),
            'message' => 'Reset Link Sent'),
            200
        );

            case Password::INVALID_USER:
                return Response::json(array(
            'email' => trans($response),
            'message' => 'This is an invalid Email'),
            401
        );
    }

    }

    public function getReset($token)
    {
        if (is_null($token)) {
            throw new NotFoundHttpException;
        }

        return view('auth.reset')->with('token', $token);
        //return redirect()->away('http://localhost/repos/angular/templates/reset_password.html/'.$token);
        //when creating the reset password form the get token should be dynamically attached then sent back in post to the next controller
        //basically we need to do dynamic routing in angularjs to accept the modified url and the use routeparams in angular to read the url and send back the token
    }

    public function postReset(Request $request)
    {
        $this->validate($request, [
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed|min:6',
        ]);

        $credentials = $request->only(
            'email', 'password', 'password_confirmation', 'token'
        );

        $response = Password::reset($credentials, function ($user, $password) {
            $this->resetPassword($user, $password);
        });

        switch ($response) {
            case Password::PASSWORD_RESET:
                return redirect()->away('http://localhost/repos/angular/templates/login.html');

            default:
                 return Response::json(array(
            'error' => false,
            'message' => 'Password Reset Unsucessful'),
            200
        );
        }
    }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Contracts\Auth\CanResetPassword  $user
     * @param  string  $password
     * @return void
     */
    protected function resetPassword($user, $password)
    {
        $user->password = $password;

        $user->save();

        //Auth::login($user);
    }




//end
}
