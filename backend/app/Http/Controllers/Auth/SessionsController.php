$credentials = [
            'username' => Input::get('username'),
            'password' => Input::get('password'),
            'confirmed' => 1
        ];

        if ( ! Auth::attempt($credentials))
        {
            return Redirect::back()
                ->withInput()
                ->withErrors([
                    'credentials' => 'We were unable to sign you in.'
                ]);
        }

        Flash::message('Welcome back!');

        return Redirect::home();
    }


    -------------continued password controller

    public function postEmail(Request $request)
    {
        $this->validate($request, ['email' => 'required|email']);

        $response = Password::sendResetLink($request->only('email'), function (Message $message) {
            $message->subject($this->getEmailSubject());
        });

        switch ($response) {
            case Password::RESET_LINK_SENT:
                /*return response(array('msg' => 'Login Successful'), 200)
                ->header('Content-Type', 'application/json');*/
                return Response::json(array(
            'status' => trans($response),
            'message' => 'Reset Link Sent'),
            200
        );

            case Password::INVALID_USER:
                return Response::json(array(
            'email' => trans($response)
            'message' => 'This is an invalid Email'),
            401
        );
    }

    }

    public function getReset($token)
    {
        if (is_null($token)) {
            throw new NotFoundHttpException;
        }

        /*return view('auth.reset')->with('token', $token);*/
        return redirect()->away('http://localhost/repos/angular/templates/reset_password.html'.$token);
    }

    public function postReset(Request $request)
    {
        $this->validate($request, [
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed|min:6',
        ]);

        $credentials = $request->only(
            'email', 'password', 'password_confirmation', 'token'
        );

        $response = Password::reset($credentials, function ($user, $password) {
            $this->resetPassword($user, $password);
        });

        switch ($response) {
            case Password::PASSWORD_RESET:
                return Response::json(array(
            'status' => trans($response),
            'message' => 'Password Reset Successful'),
            200
        );

            default:
                return Response::json(array(
            'email' => trans($response),
            'message' => 'Password Reset Error'),
            401
        );
        }

    }