<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*METHOD 1 OF PROTECTING ROUTES*/
/*Route::group(array('before' => 'auth'), function() 
{
    //PROTECTED ROUTES HERE
   
	Route::get('/home', function () {
	    return view('welcome');
	});


	Route::get('user/{id}', function ($id) {
	    return 'User '.$id;
});

}*/

/*Route::group(array('before' => 'auth'), function() 
{*/


/*Route::get('/dash1', function () {
	    return view('dash/dash1');
	});*/


/*METHOD 2 OF PROTECTING ROUTES*/
// Authentication routes. No extra protection required for them

//repos\signup\vendor\laravel\framework\src\Illuminate\Foundation\Auth\AuthenticatesUsers.php
//Route::get('api/auth/login', 'Auth\AuthController@getLogin');
/*Route::get('auth/login2', 'Auth\AuthController@getLogin');*/
//Route::post('api/auth/login', 'Auth\AuthController@postLogin');

/*Route::get('token', function () {  
    return Session::token();
});*/

Route::group(['prefix' => 'api','middleware' => 'cors'],function(){

	

	Route::get('auth/login', 'Auth\AuthController@getLogin');
	Route::post('auth/login', 'Auth\AuthController@postLogin');
	
	Route::get('auth/logout', 'Auth\AuthController@logout');
		// Registration routes...
	Route::get('auth/register', 'Auth\AuthController@getRegister');
	Route::post('auth/register', 'Auth\AuthController@postRegister');

	//email confirmation route
	Route::get('auth/register/confirm/{token}', 'Auth\AuthController@confirmEmail');



	// Password reset link request routes...
	Route::get('password/email', 'Auth\PasswordController@getEmail');
	Route::post('password/email', 'Auth\PasswordController@postEmail');



	// Password reset routes...
	Route::get('password/reset/{token}', 'Auth\PasswordController@getReset'); //WHEN RECIEVED BACK FROM THE EMAIL SENT TO THEM
	Route::post('password/reset', 'Auth\PasswordController@postReset');

	
});

/*Route::group(array('before'=>'login','prefix'=>'/api'),function(){
     
  
});*/


Route::get('/', function()
{
	return View::make('index'); 
}); 

Route::get('/emailverify', function()
{
	return View::make('emails/confirm'); 
});

Route::get('/emailverify2', function()
{
	return View::make('emails/confirm2'); 
}); 


Route::get('/dash1', function()
{
	return View::make('dash/dash1'); 
}); 




?>