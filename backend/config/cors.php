<?php
return [
    /*
     |--------------------------------------------------------------------------
     | Laravel CORS
     |--------------------------------------------------------------------------
     |
     | allowedOrigins, allowedHeaders and allowedMethods can be set to array('*') 
     | to accept any value, the allowed methods however have to be explicitly listed.
     |
     */
    /*'supportsCredentials' => false,
    'allowedOrigins' => ['*'],
    'allowedHeaders' => ['*'],
    'allowedMethods' => ['GET', 'POST', 'PUT',  'DELETE'],
    'exposedHeaders' => [],
    'maxAge' => 2800,
    'hosts' => [],*/


'defaults' => array(
  'supportsCredentials' => false,
  'allowedOrigins' => array(),
  'allowedHeaders' => array(),
  'allowedMethods' => array(),
  'exposedHeaders' => array(),
  'maxAge' => 0, 'hosts' => array(),
),

'paths' => array(
  'api/*' => array( 'allowedOrigins' => array('*'),
  'allowedHeaders' => array('*'),
  'allowedMethods' => array('*'),
  'maxAge' => 3600,
),
'*' => array(
  'allowedOrigins' => array('*'),
  'allowedHeaders' => array('Content-Type'),
  'allowedMethods' => array('POST', 'PUT', 'GET', 'DELETE'),
  'maxAge' => 3600,
  'hosts' => array('api.*'),
  ),
),

];