<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUsersColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function($table)
    {
        /*$table->string('new_col_name');*/
        $table->boolean('verified')->default(false);
        $table->string('token')->nullable();
     });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function($table)
    {
       /*$table->dropColumn('new_col_name');*/
    });
    }
}
