// app/database/seeds/UserTableSeeder.php
<?php

class UserTableSeeder extends Seeder
{
  public function run()
  {
    DB::table('users')->delete();
    
    User::create(array(
    	'name'     => 'Rohan',
        'username' => 'rohankar93',
        'email'    => 'rohankar99@gmail.com',
        'password' => Hash::make('rohan'),
    ));
  }
}