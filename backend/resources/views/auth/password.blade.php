<html>
@include('errors')

<form method="POST" action="/api/password/email">
    
<div>
    Token: 
    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

</div>
    <div>
        Email
            <input type="email" name="email" value="{{ old('email') }}">
    </div>

    <div>
        <button type="submit">
            Send Password Reset Link
        </button>
    </div>

    </form>

<ul>
    @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach
</ul>
</html>
