<html>

<form method="POST" action="/api/password/reset">
     
    <input type="hidden" name="token" value="{{ $token }}"> 

    <!-- <div>
    Token: Invisible field
    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
    </div> -->

    <div>
        Registered email    <input type="email" name="email" placeholder="email" value="{{ old('email') }}">
    </div>

    <div>
        New password    <input type="password" placeholder="new password" name="password">
    </div>

    <div>
        Re-confirm the New Password     <input type="password" placeholder="confirm password" name="password_confirmation">
    </div>

    <div>
        <button type="submit">
            Reset Password
        </button>
    </div>
</form>

<ul>
    @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach
</ul>
</html>


