<html>
@include('errors')

<form method="POST" action="/api/auth/register">
    <!-- <input type="hidden" name="_token" value="{{ csrf_token() }}">
    -->

    <div>
    Token: 
    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
     
    </div>
    <div>
        First Name
        <input type="text" name="first_name" value="{{ old('first_name') }}">
    </div>

    <div>
       Last Name
        <input type="text" name="last_name" value="{{ old('last_name') }}">
    </div>

    <div>
        Email
        <input type="email" name="email" value="{{ old('email') }}">
    </div>

    <div>
        Mobile
        <input type="text" name="mobile_number">
    </div>


    <div>
        Password
        <input type="password" name="password">
    </div>
    <div>
        Confirm Password
        <input type="password" name="password_confirmation">
    </div>
    <div>
        Company
        <input type="text" name="company_name">
    </div>

    <div>
        Role
        <input type="text" name="role_name">
    </div>
    <div>
        Country
        <input type="text" name="country">
    </div>

    <div>
        <button type="submit">Register</button>
    </div>
</form>

<ul>
    @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach
</ul>
</html>