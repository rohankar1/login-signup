<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Sign Up Confirmation</title>
</head>
<body>
    <h1>Thanks for signing up!</h1>

    <p>
        To complete the process, click the link to <a href='{{ url("api/auth/register/confirm/{$user->token}") }}'>Confirm your email address</a> real quick!
    </p>
</body>
</html>