# README #

This README documents whatever steps are necessary to get the Login and Signup Application Running as well as any developer communication.

### What is this repository for? ###

* Contains the Separated Backend and Frontend code for the application
* v1.0 of the Application
* Laravel framework 5.1
* Angular Framework 1.4.x

### How do I get set up? ###

* Setup Backend Laravel first by configuring database mySQL with valid creds 
* migrate tables to the created DB using "php artisan migrate" command
* Setup the Mail credentials under mailer and Mail.php
* Setup the service.php
* Setup the user.php
* Host the application on laravel server eg localhost:8000 by php artisan serve command
* Check the backend functionality to make sure connection is OK
* Deploy angular routes over WAMP or XAMP server localhost
* pass HTTP requests between the 2 separated servers.

### Coding guidelines & Milestones###

* Followed Nextorbit coding practices for web applications located on Confluence
* Code review done for front end validations and Views
* Code review done for API restful testing

###Current Work

* Getting Tested
* Mailer template image issue: SOLUTION

HTML::image() generates absolute path to image, like this:

http://yoursite.dev/image.png
which works perfectly in your local environment.

Mail client, as Gmail, will override image path with something like this:

http://images.gmail.com/?src=http://yoursite.dev/image.png
Obviosly 3rd party service can't access your local domain and so you see broken image.

As a solution, you can upload images to 3rd party service like Amazon AWS and pass link to uploaded image to email template.

AND 

http://laravel.com/docs/5.0/mail#embedding-inline-attachments

###Important Information

* Dynamic Routing with Angularjs:

The password reset token and dynamic routing via angularjs view is required to complete this project. Currently password reset form is fetched via server.

* X-XSRF-TOKEN LARAVEL 5.1 DOC:

Laravel also stores the CSRF token in a XSRF-TOKEN cookie. You can use the cookie value to set the X-XSRF-TOKEN request header. Some JavaScript frameworks, like Angular, do this automatically for you. It is unlikely that you will need to use this value manually.

NOTE: Allows the XSRF token to pass through the API and hence we should face no token mismatches


* Transforming Requests and Responses ANGULARJS DOC:

Both requests and responses can be transformed using transformation functions: transformRequest and transformResponse. These properties can be a single function that returns the transformed value (function(data, headersGetter, status)) or an array of such transformation functions, which allows you to push or unshift a new transformation function into the transformation chain.

Default Transformations
The $httpProvider provider and $http service expose defaults.transformRequest and defaults.transformResponse properties. If a request does not provide its own transformations then these will be applied.

You can augment or replace the default transformations by modifying these properties by adding to or replacing the array.

Angular provides the following default transformations:

Request transformations ($httpProvider.defaults.transformRequest and $http.defaults.transformRequest):

If the data property of the request configuration object contains an object, serialize it into JSON format.
Response transformations ($httpProvider.defaults.transformResponse and $http.defaults.transformResponse):

If XSRF prefix is detected, strip it (see Security Considerations section below).
If JSON response is detected, deserialize it using a JSON parser.


### References and Contributions ###

* https://github.com/stevenh77/AngularValidationWithNgMessage
* http://bensmith.io/email-verification-with-laravel
* https://nextorbit.atlassian.net/wiki/display/~ripunjoy.sonowal/AngularJS+Coding+Guidelines